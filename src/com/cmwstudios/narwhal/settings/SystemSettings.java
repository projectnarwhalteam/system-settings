package com.cmwstudios.narwhal.settings;

import javax.swing.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SystemSettings extends javax.swing.JDialog {
    private javax.swing.JPanel contentPane;
    private javax.swing.JButton buttonOK;
    private javax.swing.JButton buttonCancel;
    private JTabbedPane tabbedPane1;
    private JCheckBox allowMusicCheckBox;
    private JCheckBox allowBugsnagReportingCheckBox;
    private JTextField nameTextField;
    private JTextField organizationTextField;

    public SystemSettings() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onCancel();
            }
        }, javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0), javax.swing.JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        nameTextField.setText(readProp("UserName"));
        organizationTextField.setText(readProp("UserOrg"));
        if (readProp("AllowBugSnag") == "true") {
            allowBugsnagReportingCheckBox.setSelected(true);
        } else {
            allowBugsnagReportingCheckBox.setSelected(false);
        }
        if (readProp("AllowMusic") == "true") {
            allowMusicCheckBox.setSelected(true);
        } else {
            allowMusicCheckBox.setSelected(false);
        }
    }

    public static void main(String[] args) {
        SystemSettings dialog = new SystemSettings();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void onOK() {
        if (allowMusicCheckBox.isSelected()) {
            setProp("AllowMusic", "true");
        } else {
            setProp("AllowMusic", "false");
        }
        if (allowBugsnagReportingCheckBox.isSelected()) {
            setProp("AllowBugsnag", "true");
        } else {
            setProp("AllowBugsnag", "false");
        }
        setProp("UserName", nameTextField.getText());
        setProp("UserOrg", organizationTextField.getText());
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    private void setProp(String key, String value) {
        Properties prop = new Properties();
        InputStream in = getClass().getResourceAsStream("narwhal.properties");
        try {
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        prop.setProperty(key, value);
    }

    private String readProp(String key) {
        Properties prop = new Properties();
        InputStream in = getClass().getResourceAsStream("xyz.properties");
        try {
            prop.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(key);
    }
}
